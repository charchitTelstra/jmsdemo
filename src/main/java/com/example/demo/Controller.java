package com.example.demo;

import static org.assertj.core.api.Assertions.assertThat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.rule.OutputCapture;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {
	@Autowired
	private Producer producer;
	@RequestMapping("/send")
	public void sendMessage(String message) throws InterruptedException {
		this.producer.send("Test Message");
		//send method is unable to use a referance of string hence passing directly the string message
	}
}
